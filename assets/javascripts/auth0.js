/* global Auth0Lock */
(function () {
  function appendScript(src, callback) {
    var new_script = document.createElement('script');
    new_script.setAttribute('src',src);
    new_script.onload = callback;
    document.head.appendChild(new_script);
  }

  var lock;

  var script_url = '//cdn.auth0.com/js/lock-9.2.js';

  appendScript(script_url, function () {
    var checkInterval = setInterval(function () {
      if (!Discourse.SiteSettings) {
		console.log("Discourse SiteSettings not found!");
        return;
      }

      clearInterval(checkInterval);

      if (!Discourse.SiteSettings.auth0_client_id) {
		console.log("No discourse site settings");
        return;
      }

      var client_id = Discourse.SiteSettings.auth0_client_id;
      var domain = Discourse.SiteSettings.auth0_domain;
		
	  console.log("client_id " + client_id);
	  console.log("domain " + domain);
	  
	  
      lock = new Auth0Lock(client_id, domain);

    }, 300);
  });

  var LoginController = require('discourse/controllers/login').default;
  LoginController.reopen({
    authenticationComplete: function () {
	  console.log("arguments ", arguments);
      if (lock) {
        lock.hide();
      }
      return this._super.apply(this, arguments);
    }
  });

  var ApplicationRoute = require('discourse/routes/application').default;
  ApplicationRoute.reopen({
    actions: {
      showLogin: function() {
	    console.log("show login called");
        if (!Discourse.SiteSettings.auth0_client_id || Discourse.SiteSettings.auth0_connection !== '') {
          return this._super();
        }

		console.log("got past first stopper");
		
        lock.show({
          popup:        true,
          responseType: 'code',
          callbackURL:  Discourse.SiteSettings.auth0_callback_url
        });

		console.log("got past third stopper");
        this.controllerFor('login').resetForm();
      },
      showCreateAccount: function () {
	    console.log("show create account");
        if (!Discourse.SiteSettings.auth0_client_id || Discourse.SiteSettings.auth0_connection !== '') {
          return this._super();
        }

		console.log("got past first stopper");
		
        var createAccountController = Discourse.__container__.lookup('controller:createAccount');

        if (createAccountController && createAccountController.accountEmail) {
		  console.log("got past the createAccountController");
          if (lock) {
			console.log("hiding lock as true");
            lock.hide();
            Discourse.Route.showModal(this, 'createAccount');
          } else {
			console.log("calling this.super()");
            this._super();
          }
        } else {
		  console.log("calling lock show signup");
          lock.show({
            mode:         'signup',
            popup:        true,
            responseType: 'code',
            callbackURL:  Discourse.SiteSettings.auth0_callback_url
          });
        }
      }
    }
  });

})();
